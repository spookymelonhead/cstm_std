/*
 * project: breadbutter, generic
 * brief: plug and play, simple application modules.
 * tags: standard return types, macros, 
 * Copyright (c) 2017 - 2025.
 * All Rights Reserved.
 * 
 * creator: Hardik Madaan
 * alter-ego: Brian doofus 
 * mail: hardik.mad@gmail.com
 * web: www.tachymoron.wordpress.com
 * git: https://bitbucket.org/bacon_toast/
 *
 */
#ifndef _CUSTOM_STD_TYPE_H_
#define _CUSTOM_STD_TYPE_H_

#define USE_STD_IN_OUT

/*=============================================================================
																	File-Includes
=============================================================================*/
#include "stdlib.h"
#ifdef USE_STD_IN_OUT
#include "stdio.h"
#include "string.h"
#endif
/*=============================================================================
														  		MACROs-&-ENUMs
=============================================================================*/

#if defined USE_STD_IN_OUT
	#define PRINT_MESSAGE(x, y, z, p, q) printf(x, y, z, p, q)
#elif defined LOGGING
	#define PRINT_MESSAGE(x, y) ERROR_MSG(x,y)
#endif /* USE_STD_IN_OUT */

/*----------------------------------------------------------------------------*/
/*!@brief macro to define NULL
  !@details SAFETYMCUSW 218 S MR:20.2 <APPROVED> "Custom Type Definition." */
#ifndef NULL
  #define NULL ((void *)0U)
#endif /* NULL */

/*----------------------------------------------------------------------------*/
/*!@brief macro to define TRUE */
#ifndef TRUE
  #define TRUE true
#endif /* TRUE */

/*----------------------------------------------------------------------------*/
/*!@brief macro to define FALSE */
#ifndef FALSE
  #define FALSE false
#endif /* FALSE */

/*----------------------------------------------------------------------------*/
/*!@brief macro to define ENABLE */
#ifndef ENABLE
  #define ENABLE 1U
#endif /* ENABLE */

/*----------------------------------------------------------------------------*/
/*!@brief macro to define DISABLE */
#ifndef DISABLE
  #define DISABLE 0U
#endif /* DISABLE */

/*=============================================================================
							 									Type-Definitions
=============================================================================*/
/*@brief:
*/
#ifndef CUSTOM_STD_RETURN_TYPE
	#define CUSTOM_STD_RETURN_TYPE
	typedef enum 
	{
	  RT_INVALID 									= -8,
	  RT_INVALID_SEQ							= -7,
	  RT_INVALID_ARG							= -6,
	  RT_NULL_PTR									= -5,
	  RT_MALLOC_FAILED		      	= -4,
	  RT_NOT_SUPPORTED						= -3,
	  RT_INVALID_CONFIG					 	= -2,
	  RT_FAILURE                  = -1,
	  RT_SUCCESS								  =  0,

	  CSTM_STD_RT_NUM             = RT_SUCCESS - RT_INVALID		
	}cstm_std_return_t;
#endif/* CUSTOM_STD_RETURN_TYPE */

#ifdef CUSTOM_STD_RETURN_TYPE
	#define rt cstm_std_return_t
#endif /* CUSTOM_STD_RETURN_TYPE */

/*=============================================================================
							 							Extern-Declarations
=============================================================================*/

/*=============================================================================
							 						 Function-Declarations
=============================================================================*/

#define CHECK_STATUS(x) 																																														\
	do{																																																								\
			if(RT_SUCCESS != x) 																																													\
			{																																																							\
				PRINT_MESSAGE("\nerror code:%d\nfile: %s\n func: %s\nline: %d\nEXIT\n", x, __FILE__, __FUNCTION__, __LINE__);\
				exit(0);																																																		\
			}																																																							\
			else																																																					\
			{																																																							\
																																																										\
			}																																																							\
	}while(0)																																																					\

#endif /* _CUSTOM_STD_TYPE_H_ */